# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|


# provision docker host No:1 in aws
  config.vm.define "d1host" do |host1|
    host1.vm.box = "dummy"

    host1.vm.provider :aws do |aws, override|
        aws.aws_profile = "default"
        aws.keypair_name = "dob-exam-key"
        aws.region = "eu-west-1"

        aws.ami = "ami-3548444c"
        aws.instance_type = "t2.micro"
        aws.user_data = File.read("provision-common.sh")
        aws.subnet_id = "subnet-0633ab60"
        aws.security_groups = [
            "sg-095a6f8410a721146",
            "sg-073badc8af78cd6a4",
            "sg-0e75b17210b7a93bb"
            ]
        aws.availability_zone = "eu-west-1b"
        aws.private_ip_address = "172.31.0.104"
        aws.iam_instance_profile_name="default_iam_ec2_role"
        aws.tags = {
            'Name' => 'd1host.sulab.exam',
            'Description' => 'Docker Swarm Host No:1 (DevOpsBasics Exam)'
        }

        override.ssh.username = "centos"
        override.ssh.private_key_path = "~/keys/dob-exam-key.pem"
        override.vm.synced_folder ".", "/vagrant", disabled: true
    end
  end

# provision the docker No:2 in aws
  config.vm.define "d2host" do |host2|
    host2.vm.box = "dummy"

    host2.vm.provider :aws do |aws, override|
        aws.aws_profile = "default"
        aws.keypair_name = "dob-exam-key"
        aws.region = "eu-west-1"

        aws.ami = "ami-3548444c"
        aws.instance_type = "t2.micro"
        aws.user_data = File.read("provision-common.sh")
        aws.subnet_id = "subnet-0633ab60"
        aws.security_groups = [
            "sg-095a6f8410a721146",
            "sg-073badc8af78cd6a4",
            "sg-0e75b17210b7a93bb"
            ]
        aws.availability_zone = "eu-west-1b"
        aws.private_ip_address = "172.31.0.105"
        aws.iam_instance_profile_name="default_iam_ec2_role"
        aws.tags = {
            'Name' => 'd2host.sulab.exam',
            'Description' => 'Docker Swarm Host No:2 (DevOpsBasics Exam)'
        }

        override.ssh.username = "centos"
        override.ssh.private_key_path = "~/keys/dob-exam-key.pem"
        override.vm.synced_folder ".", "/vagrant", disabled: true
    end
  end

# provision the docker No:3 in aws
  config.vm.define "d3host" do |host3|
    host3.vm.box = "dummy"

    host3.vm.provider :aws do |aws, override|
        aws.aws_profile = "default"
        aws.keypair_name = "dob-exam-key"
        aws.region = "eu-west-1"

        aws.ami = "ami-3548444c"
        aws.instance_type = "t2.micro"
        aws.user_data = File.read("provision-common.sh")
        aws.subnet_id = "subnet-0633ab60"
        aws.security_groups = [
            "sg-095a6f8410a721146",
            "sg-073badc8af78cd6a4",
            "sg-0e75b17210b7a93bb"
            ]
        aws.availability_zone = "eu-west-1b"
        aws.private_ip_address = "172.31.0.106"
        aws.iam_instance_profile_name="default_iam_ec2_role"
        aws.tags = {
            'Name' => 'd3host.sulab.exam',
            'Description' => 'Docker Swarm Host No:3 (DevOpsBasics Exam)'
        }

        override.ssh.username = "centos"
        override.ssh.private_key_path = "~/keys/dob-exam-key.pem"
        override.vm.synced_folder ".", "/vagrant", disabled: true
    end
  end

# provision the jenkins host in aws
  config.vm.define "jhost" do |host4|
    host4.vm.box = "dummy"

    host4.vm.provider :aws do |aws, override|
        aws.aws_profile = "default"
        aws.keypair_name = "dob-exam-key"
        aws.region = "eu-west-1"

        aws.ami = "ami-3548444c"
        aws.instance_type = "t2.micro"
        aws.user_data = File.read("provision-common.sh")
        aws.subnet_id = "subnet-0633ab60"
        aws.security_groups = [
            "sg-095a6f8410a721146",
            "sg-08616bdedc0e8b4b5"
            ]
        aws.availability_zone = "eu-west-1b"
        aws.private_ip_address = "172.31.0.103"
        aws.elastic_ip = "34.251.192.1"
        aws.iam_instance_profile_name="default_iam_ec2_role"
        aws.tags = {
            'Name' => 'jhost.sulab.exam',
            'Description' => 'Jenkins Host (DevOpsBasics Exam)'
        }

        override.ssh.username = "centos"
        override.ssh.private_key_path = "~/keys/dob-exam-key.pem"
        override.vm.synced_folder ".", "/vagrant", disabled: true
    end
  end

# provision the nagios host in aws
  config.vm.define "nhost" do |host5|
    host5.vm.box = "dummy"

    host5.vm.provider :aws do |aws, override|
        aws.aws_profile = "default"
        aws.keypair_name = "dob-exam-key"
        aws.region = "eu-west-1"

        aws.ami = "ami-3548444c"
        aws.instance_type = "t2.micro"
        aws.user_data = File.read("provision-common.sh")
        aws.subnet_id = "subnet-0633ab60"
        aws.security_groups = [
            "sg-095a6f8410a721146",
            "sg-0e75b17210b7a93bb"
            ]
        aws.availability_zone = "eu-west-1b"
        aws.private_ip_address = "172.31.0.102"
        aws.elastic_ip = "63.33.25.176"
        aws.iam_instance_profile_name="default_iam_ec2_role"
        aws.tags = {
            'Name' => 'nhost.sulab.exam',
            'Description' => 'Nagios Host (DevOpsBasics Exam)'
            }

        override.ssh.username = "centos"
        override.ssh.private_key_path = "~/keys/dob-exam-key.pem"
        override.vm.synced_folder ".", "/vagrant", disabled: true
    end
  end

# provision the ansible host in aws
  config.vm.define "ahost" do |host6|
    host6.vm.box = "dummy"

    host6.vm.provider :aws do |aws, override|
        aws.aws_profile = "default"
        aws.keypair_name = "dob-exam-key"
        aws.region = "eu-west-1"

        aws.ami = "ami-3548444c"
        aws.instance_type = "t2.micro"
        aws.user_data = File.read("provision-common.sh")
        aws.subnet_id = "subnet-0633ab60"
        aws.security_groups = ["sg-095a6f8410a721146"]
        aws.availability_zone = "eu-west-1b"
        aws.private_ip_address = "172.31.0.101"
        aws.iam_instance_profile_name="default_iam_ec2_role"
        aws.tags = {
            'Name' => 'ahost.sulab.exam',
            'Description' => 'Ansible Host (DevOpsBasics Exam)'
        }

        override.ssh.username = "centos"
        override.ssh.private_key_path = "~/keys/dob-exam-key.pem"
        override.vm.synced_folder ".", "/vagrant", disabled: true
    end
  end


end
