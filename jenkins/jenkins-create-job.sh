#!/bin/bash

# 
# Create Jenkins create job
# 

cat <<EOF | java -jar /opt/jenkins-cli.jar -s http://localhost:8080/ create-job Swarm-GitHub-Final --username admin --password admin
<?xml version='1.1' encoding='UTF-8'?>
<project>
  <actions/>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties>
    <com.coravy.hudson.plugins.github.GithubProjectProperty plugin="github@1.29.3">
      <projectUrl>https://github.com/shekeriev/dob-2018-sep-exam-1.git/</projectUrl>
      <displayName></displayName>
    </com.coravy.hudson.plugins.github.GithubProjectProperty>
  </properties>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@3.9.1">
    <configVersion>2</configVersion>
    <userRemoteConfigs>
      <hudson.plugins.git.UserRemoteConfig>
        <url>https://github.com/shekeriev/dob-2018-sep-exam-1.git</url>
      </hudson.plugins.git.UserRemoteConfig>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name>*/master</name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <submoduleCfg class="list"/>
    <extensions/>
  </scm>
  <assignedNode>d1host.sulab.exam</assignedNode>
  <canRoam>false</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
  <triggers>
    <hudson.triggers.SCMTrigger>
      <spec>H/2 * * * *</spec>
      <ignorePostCommitHooks>false</ignorePostCommitHooks>
    </hudson.triggers.SCMTrigger>
  </triggers>
  <concurrentBuild>false</concurrentBuild>
  <customWorkspace>/home/centos</customWorkspace>
  <builders>
    <hudson.tasks.Shell>
      <command>cd /home/centos/php
docker image build -t img-php .</command>
    </hudson.tasks.Shell>
    <hudson.tasks.Shell>
      <command>cd /home/centos/mysql
docker image build -t img-mysql .</command>
    </hudson.tasks.Shell>
    <hudson.tasks.Shell>
      <command>cd /home/centos/nginx
docker image build -t img-nginx .</command>
    </hudson.tasks.Shell>
    <hudson.tasks.Shell>
      <command>docker stack rm exam-app &amp;&amp; sleep 20 || true
docker stack deploy --compose-file /softuni/docker/dob-exam-stack-02.yml exam-app</command>
    </hudson.tasks.Shell>
  </builders>
  <publishers/>
  <buildWrappers/>
</project>
EOF
